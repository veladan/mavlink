//
//  MavlinkChannel.swift
//  mavlink
//
//  Created by Daniel Vela on 17/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation

class MavlinkChannel : ostream {
    var parser: MavlinkParser
    var source: MavlinkSource
    var logOutput: FileOutputStream?
    
    init(parser: MavlinkParser, source: MavlinkSource) {
        self.parser = parser
        self.source = source
        let fileManager = MavlinkFileManager()
        let fileName = fileManager.createFileName()
        if let filePath = fileManager.filePathForFile(fileName) {
            do {
                self.logOutput = try FileOutputStream(filePath: filePath)
            } catch (FileOutputStream.FileOutputStreamError.fileCantBeOpened) {
                print("File can be opened to write")
            } catch {
                print("File fail to open to write")
            }

        }
    }
    
    func writeInt(_ char: UInt8) {
        self.logOutput?.writeInt(char)
        self.parser.charReceived(char, source: self.source)
    }
}
