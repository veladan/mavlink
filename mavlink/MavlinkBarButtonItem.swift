//
//  MavlinkBarButtonItem.swift
//  mavlink
//
//  Created by Daniel Vela on 21/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit

class MavlinkBarButtonItem: UIBarButtonItem {

    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        startTimer()
    }
    
    func startTimer() {
        var _ = Timer.scheduledTimer(timeInterval: 1.4, target: self, selector: #selector(MavlinkBarButtonItem.timerTick), userInfo: nil, repeats: true)
    }
    
    @objc func timerTick() {
        
    }

}
