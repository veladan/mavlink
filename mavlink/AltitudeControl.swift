//
//  AltitudeControl.swift
//  mavlink
//
//  Created by Daniel Vela on 18/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit

class AltitudeControl: MavlinkBarButtonItem {
    
    @objc override func timerTick() {
        
        guard let rawAltitude = Database.get().rawAltitude() else {
            setText("Alt: --")
            return
        }
        
        let altitude = rawAltitude / 1000
        let str = "Alt: \(altitude) m"
        setText(str)
    }
    
    func setText(_ str: String) {
        self.title = str
    }

}
