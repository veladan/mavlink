//
//  BorraMeViewController.m
//  mavlink
//
//  Created by Daniel Vela on 02/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

#import "BorraMeViewController.h"

#include "mavlink.h"

@interface BorraMeViewController ()

@end

@implementation BorraMeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    mavlink_msg_get_send_buffer_length(nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
