//
//  ProgressControl.swift
//  mavlink
//
//  Created by Daniel Vela on 21/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit

class ProgressControl: UISlider, ProgressDelegate {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    var maxStep: Int?
    func start(_ maxStep: Int) {
        DispatchQueue.main.async {
            self.maxStep = maxStep
            self.isHidden = false
            self.minimumValue = 0
            self.maximumValue = Float(maxStep)
            self.value = 0
        }
    }

    func step(_ currentStep: Int) {
        Thread.sleep(forTimeInterval: 0.00001)
        DispatchQueue.main.async {
            self.value = Float(currentStep)
        }
    }

    func stop() {
        self.isHidden = true
        self.value = 0
    }
}
