//
//  FileOutputStream.swift
//  mavlink
//
//  Created by Daniel Vela on 10/07/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation

class FileOutputStream: ostream {
    var file: FileHandle?

    enum FileOutputStreamError: Error {
        case fileCantBeOpened
    }
    
    init(filePath: String) throws {
        file = FileHandle(forWritingAtPath: filePath)
        if file == nil {
            FileManager.default.createFile(atPath: filePath, contents: nil, attributes: nil)
            file = FileHandle(forWritingAtPath: filePath)
            if file == nil {
                throw FileOutputStreamError.fileCantBeOpened
            }
        }
    }
    
    func writeInt(_ char: UInt8) {
        let array = [UInt8](repeating: char, count: 1)
        let data = Data(bytes: UnsafePointer<UInt8>(array), count: 1)
        file?.write(data)
    }
}
