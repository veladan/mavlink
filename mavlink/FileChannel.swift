//
//  FileChannel.swift
//  mavlink
//
//  Created by Daniel Vela on 17/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation

protocol ProgressDelegate {
    func start(_ maxStep: Int)
    func step(_ currentStep: Int)
    func stop()
}

class FileChannel {
    var delegate: ProgressDelegate?
    var input: istream
    var parser: MavlinkParser
    var stopped = false
    
    init(input: istream, parser:MavlinkParser) {
        self.input = input
        self.parser = parser
    }
    
    func run() {
        let fileLength = input.lengthAvailable()
        delegate?.start(fileLength)
        while !stopped && input.isDataAvailable() {
            let (char, currentStep) = input.readInt()
            self.parser.charReceived(char, source: MavlinkSource.file)
            delegate?.step(currentStep)
        }
        self.parser.didFinish()
        delegate?.stop()
    }
    
    func stop() {
        stopped = true
    }
    
    func movePointerTo(_ position: Float) {
        input.movePointerTo(Int(position))
    }
}
