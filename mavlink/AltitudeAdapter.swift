//
//  AltitudeAdapter.swift
//  mavlink
//
//  Created by Daniel Vela on 18/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation

class AltitudeAdapter {
    static func getAltitude() -> String {
        let altitude = Database.get().rawAltitude()
        
        var value = ""
        if let altitude = altitude {
            let altitudeInMeters = altitude / 1000
//            value = String(format: "%f", altitudeInMeters)
            value = "\(altitudeInMeters)"
        } else {
            value = "--"
        }
        return value
    }
}