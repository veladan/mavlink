//
//  BatteryControl.swift
//  mavlink
//
//  Created by Daniel Vela on 18/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit

class BatteryControl: MavlinkBarButtonItem {

    @objc override func timerTick() {
        guard let rawBattery = Database.get().rawBattery() else {
            setText("Bat: --")
            return
        }
        
        let battery = Double(rawBattery) / 1000.0
        let str = "Bat: ".appendingFormat("%.1f V", battery)
        setText(str)
    }
    
    func setText(_ str: String) {
        self.title = str
    }
}
