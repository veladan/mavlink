//
//  MavlinkFileManager.swift
//  mavlink
//
//  Created by Daniel Vela on 20/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit

protocol OSDataSource {
    associatedtype Element
    var count: Int { get }
    subscript(index: Int) -> Element { get set }
    func insert(_ element: Element, at: Int)
    func post(_ element: Element)
    func remove(_ indexes: [Int])
}

class MavlinkFileManager: NSObject, OSDataSource {
    
    var fileArray:[String]

    var count: Int {
        get {
            return fileArray.count
        }
    }
    
    subscript(index: Int) -> String {
        get {
            guard (index < fileArray.count && index >= 0) else {return ""}
            return fileArray[index]
        }
        set {
            fileArray[index] = newValue
        }
    }
    
    func insert(_ element: String, at: Int) {
        fileArray.insert(element, at: at)
    }
    
    func post(_ element: String) {
        fileArray.append(element)
    }
    
    func removeOne(_ index: Int) {
        // Delete the file
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let path = documentPath + "/\(fileArray[index])"
        do {
        try FileManager().removeItem(atPath: path)
        } catch {
            print("File can't be deleted")
        }
        
        fileArray.remove(at: index)
    }
    
    func remove(_ indexes: [Int]) {
        for index in indexes {
            let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let path = documentPath + "/\(fileArray[index])"
            do {
                try FileManager().removeItem(atPath: path)
            } catch {
                print("File can't be deleted")
            }
        }
        self.loadArray()
    }

    func loadArray() {
        self.fileArray = []
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let resourcePath = paths[0]
//        let resourcePath = NSBundle.mainBundle().resourcePath
        
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(atPath: resourcePath)
            self.fileArray = directoryContents.filter({ (element: String) -> Bool in
                return element.hasSuffix(".tlog")
            })
            self.fileArray = self.fileArray.sorted(by: { (a: String, b:String) -> Bool in
                return a > b
            })
        } catch {
            print("Error accesing files in resource directory")
            
        }
    }
    
    override init() {
        self.fileArray = []
        super.init()
        self.loadArray()
    }

    func loadFile(_ index: Int) -> FileInputStream? {
        do {
            let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let path = documentPath + "/\(fileArray[index])"
            let file = try FileInputStream(fileName: path)
            return file
        } catch FileInputStream.FileInputStreamError.fileCantBeOpened {
            print("File can't be opened")
        } catch {
            print("File fail to open")
        }
        return nil
    }
    
    func getPath(_ index: Int) -> String? {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let resourcePath = paths[0]
        let path = resourcePath + "/\(fileArray[index])"
        return path
    }
    
    func createFileName() -> String {
        let now = Date()
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            let year = (calendar as NSCalendar).component(NSCalendar.Unit.year, from: now)
            let month = (calendar as NSCalendar).component(NSCalendar.Unit.month, from: now)
            let day = (calendar as NSCalendar).component(NSCalendar.Unit.day, from: now)
            let hour = (calendar as NSCalendar).component(NSCalendar.Unit.hour, from: now)
            let minute = (calendar as NSCalendar).component(NSCalendar.Unit.minute, from: now)
            let second = (calendar as NSCalendar).component(NSCalendar.Unit.second, from: now)
            let fileName = "log_bluetooth_".appendingFormat("%04d_%02d_%02d_%02d_%02d_%02d.tlog", year,month,day, hour, minute, second)
            return fileName
    }
    
    func filePathForFile(_ fileName: String) -> String? {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory + "/\(fileName)"
    }
    
    func loadInitArray() {
        self.fileArray = []
        let resourcePath = Bundle.main.resourcePath
        
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(atPath: resourcePath!)
            self.fileArray = directoryContents.filter({ (element: String) -> Bool in
                return element.hasSuffix(".tlog")
            })
            self.fileArray = self.fileArray.sorted(by: { (a: String, b:String) -> Bool in
                return a > b
        })
        } catch {
            print("Error accesing files in resource directory")
        }
    }

    
    func copyFileToDocumentsPath() {
        loadInitArray()
        let destinationPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        for fileName in fileArray {
            let sourceFile = Bundle.main.path(forResource: fileName, ofType: nil)
            let destinationFile = destinationPath + "/\(fileName)"
            do {
                try FileManager().copyItem(atPath: sourceFile!, toPath: destinationFile)
            } catch let error as NSError {
                print(error)
            }
        }
    }
}
