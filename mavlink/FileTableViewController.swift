//
//  FileTableViewController.swift
//  mavlink
//
//  Created by Daniel Vela on 21/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit

class FileTableViewController: UITableViewController {

    @IBOutlet weak var shareButton: UIBarButtonItem!
    @IBOutlet weak var deleteButton: UIBarButtonItem!
    weak var delegate: ViewController?
    lazy var files = MavlinkFileManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        shareButton.isEnabled = editing
        deleteButton.isEnabled = editing
    }

    @IBAction func sharePressed(_ sender: AnyObject) {
        presentShareActivity()
    }
    
    @IBAction func deletePressed(_ sender: AnyObject) {
        guard let indexPaths = self.tableView.indexPathsForSelectedRows else { return }
        var indexes: [Int] = []
        for indexPath in indexPaths {
            indexes.append((indexPath as NSIndexPath).row)
        }
        files.remove(indexes)
        tableView.deleteRows(at: indexPaths, with: .fade)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files.count
    }

    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileTableCell", for: indexPath)
        cell.textLabel?.text = files[(indexPath as NSIndexPath).row]
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard !self.tableView.isEditing else { return }
        guard let fileInput = files.loadFile((indexPath as NSIndexPath).row) else { return }
        delegate?.fileSelected(fileInput)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            files.remove([(indexPath as NSIndexPath).row])
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    var avc:UIActivityViewController?
    func presentShareActivity() {
        guard let indexPaths = self.tableView.indexPathsForSelectedRows else { return }
        var activityItems:[AnyObject]  = []
        for indexPath in indexPaths {
            if let path = files.getPath((indexPath as NSIndexPath).row) {
                activityItems.append(URL(fileURLWithPath: path) as AnyObject)
            }
        }
        avc = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        avc!.setValue("mavlink log files", forKey: "Subject")
        avc!.excludedActivityTypes = [UIActivityType.postToWeibo, UIActivityType.assignToContact, UIActivityType.copyToPasteboard]
        avc!.completionWithItemsHandler = { (activityType, completed, resturnedItems , activityError) in
        }
        avc!.popoverPresentationController?.barButtonItem = self.shareButton
        self.present(avc!, animated: true) {}
    }

}
