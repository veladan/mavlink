//
//  FileInputStream.swift
//  mavlink
//
//  Created by Daniel Vela on 07/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation

class FileInputStream: istream {
    var fileData: Data
    var filePointer: Int
    
    var count: Int
    var array: [UInt8]
    
    enum FileInputStreamError: Error {
        case fileCantBeOpened
    }
    
    init(fileName: String) throws {
        let tempFileData = try? Data(contentsOf: URL(fileURLWithPath: fileName))
        
        if tempFileData != nil {
            fileData = tempFileData!
            filePointer = 0
            
            count = fileData.count / MemoryLayout<Int8>.size
            array = [UInt8](repeating: 0, count: count)
            fileData.copyBytes(to: &array, count: count * MemoryLayout<UInt8>.size)
        } else {
            throw FileInputStreamError.fileCantBeOpened
        }
    }
    
    func lengthAvailable() -> Int {
        return count
    }

    func isDataAvailable() -> Bool {
        return filePointer < count
    }
    
    func readInt() -> (UInt8, Int) {
        let result = array[filePointer]
        filePointer += 1
        return (result, filePointer)
    }
    
    func movePointerTo(_ position: Int) {
        filePointer = position
    }
}
