//
//  BluetoothManager.swift
//  mavlink
//
//  Created by Daniel Vela on 17/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol BluetoothManagerDelegate {
    func didDiscoverPeripheral(_ name: String)
    func didUpdateState(_ string: String)
    func didConnectToPeripheral()
}

/**
    This class discovers and connect to bluetooth peripherals
*/
open class BluetoothManager : NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {

    let RBL_SERVICE_UUID   = "713D0000-503E-4C75-BA94-3148F18D941E"
    let RBL_CHAR_TX_UUID   = "713D0002-503E-4C75-BA94-3148F18D941E"
    let RBL_CHAR_RX_UUID   = "713D0003-503E-4C75-BA94-3148F18D941E"
    let RBL_CHAR_BAUD_UUID = "713D0004-503E-4C75-BA94-3148F18D941E"
    let BT_SERVICE_UUID   = "FFE0"
    let BT_CHAR_TX_UUID   = "FFE1"
    // Tbs Crossfire data
    let TBS_CROSSFIRE_SERVICE_UUID = "180f"
    let TBS_CROSSFIRE_CHAR_TX_UUID = "2a19"
    let TBS_CROSSFIRE_CHAR_RX_UUID = "2a19"
    
    var myCentralManager: CBCentralManager?
    var activePeripheral: CBPeripheral?
    var discoveredPeripheral: [String:CBPeripheral] = [:]
    
    var output: ostream?
    
    var delegate: BluetoothManagerDelegate?
    
    override init() {
        self.output = nil
    }
    
    init(output: ostream) {
        self.output = output
    }
    
    open func startUp() {
        myCentralManager = CBCentralManager(delegate: self, queue: nil, options: [
            CBCentralManagerOptionShowPowerAlertKey: true])
    }

    open func connectPeripheralNamed(_ name: String) {
        if let peripheral = discoveredPeripheral[name] {
            connectPeripheral(peripheral)
        } else {
            // TODO send an error to delegate
        }
    }
    
    func connectPeripheral(_ peripheral: CBPeripheral) {
        myCentralManager?.connect(peripheral, options: nil)
    }
    
    func disconnectPeripheral() {
        if let activePeripheral = self.activePeripheral {
            myCentralManager?.cancelPeripheralConnection(activePeripheral)
        }
    }

    func startDiscoverMavlinkServices() {
        let services:[CBUUID] = [CBUUID(string: RBL_SERVICE_UUID)]
        myCentralManager?.scanForPeripherals(withServices: services, options: nil)
    }
    
    func startDiscoverAllServices() {
        myCentralManager?.scanForPeripherals(withServices: nil, options: nil)
    }
    
    func stopDiscover() {
        myCentralManager?.stopScan()
        NSLog("Scanning stopped");
    }

    func periodicScan() {
        myCentralManager?.stopScan()
        myCentralManager?.scanForPeripherals(withServices: nil, options: nil)
        self.perform(#selector(BluetoothManager.periodicScan), with: nil, afterDelay: 5)
    }

    // MARK Central Manager delegate
    
    @objc open func centralManagerDidUpdateState(_ central: CBCentralManager) {
        var str = ""
        switch central.state {
        case .unknown:
            str = "unknown"
        case .poweredOff:
            str = "blueooth off"
        case .poweredOn:
            str = "bluetooth on"
            startDiscoverAllServices()
//            startDiscoverMavlinkServices()
        case .resetting:
            str = "resetting"
        case .unauthorized:
            str = "not authorized"
        case .unsupported:
            str = "not supported"
        }
        delegate?.didUpdateState(str)
        print("Central manager state: ", str)
    }
    
    @objc open func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        var name = ""
        if let n = peripheral.name {
            name = n
        } else {
            name = peripheral.identifier.uuidString
        }
        print("Discovered: ", name)
        discoveredPeripheral[name] = peripheral
        delegate?.didDiscoverPeripheral(name)
     }
    
    @objc open func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
        print("Central manager will restore state")
    }

    @objc open func centralManager(_ central: CBCentralManager,
                        didDisconnectPeripheral peripheral: CBPeripheral,
                                                error: Error?) {
        
        print("Did disconnect: ", peripheral.name!)
    }
    
    @objc open func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected peripheral", peripheral.name!)
        peripheral.delegate = self
        peripheral.discoverServices([CBUUID(string: BT_SERVICE_UUID), CBUUID(string: TBS_CROSSFIRE_SERVICE_UUID)]) //,CBUUID(string: RBL_CHAR_TX_UUID),CBUUID(string: RBL_CHAR_RX_UUID),CBUUID(string: RBL_CHAR_BAUD_UUID)])
        // FIXME Add Tbs Crossfire service
        self.activePeripheral = peripheral
        delegate?.didConnectToPeripheral()
    }
    
    @objc open func centralManager(_ central: CBCentralManager,
                              didFailToConnect peripheral: CBPeripheral,
                                                         error: Error?) {
        print("Did fail to connect ", peripheral.name!, error?.localizedDescription)
    }
    
    // MARK Peripheral delegate

    @objc open func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            print("Service discovered", service)
            peripheral.discoverCharacteristics([CBUUID(string: BT_CHAR_TX_UUID), CBUUID(string: TBS_CROSSFIRE_CHAR_TX_UUID)], for: service)
            
//            for service in peripheral.services! {
//                let theCharacteristics = [
//                    CBUUID(string: RBL_CHAR_RX_UUID),
//                    CBUUID(string: RBL_CHAR_TX_UUID),
//                    CBUUID(string: RBL_CHAR_BAUD_UUID)]
//                peripheral.discoverCharacteristics(theCharacteristics, forService: service)
//            }
        }
    }
    
    var characteristics:[String:CBCharacteristic] = [:]
    
    @objc open func peripheral(_ peripheral: CBPeripheral,
                          didDiscoverCharacteristicsFor service: CBService,
                                                               error: Error?) {
        print("Characteristic discovered", service.characteristics!)
        for characteristic in service.characteristics! {
            // Store the characteristic
            self.characteristics[characteristic.uuid.uuidString] = characteristic
        }
        // App is notified as new serial port data arrives
        if let char = self.characteristics[BT_CHAR_TX_UUID] {
            self.activePeripheral?.setNotifyValue(true, for: char)
        }
        // TX of Tbs Crossfire
        if let char = self.characteristics[TBS_CROSSFIRE_CHAR_TX_UUID] {
            self.activePeripheral?.setNotifyValue(true, for: char)
        }
    }
    
    @objc open func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {

        if (characteristic.uuid.uuidString == BT_CHAR_TX_UUID) ||
            (characteristic.uuid.uuidString == TBS_CROSSFIRE_CHAR_TX_UUID) {
            // Decode the data to get a MAVLink message
            let data: Data? = characteristic.value
            
            sendDataToStream(data)
        }
    }
    
    // MARK send data
    
    func write(data: Data) {
        
        guard let char = (self.characteristics[RBL_CHAR_RX_UUID] ??
            self.characteristics[TBS_CROSSFIRE_CHAR_RX_UUID]) else { return }
        self.activePeripheral?.writeValue(data, for: char, type: .withoutResponse)
    }
    
    // MARK parser interface
    
    func sendDataToStream(_ data: Data?) {
        let count = data!.count / MemoryLayout<Int8>.size
        var array = [UInt8](repeating: 0, count: count)
        (data! as NSData).getBytes(&array, length: count * MemoryLayout<UInt8>.size)
        
        for char in array {
            output?.writeInt(char)
        }
    }
}
