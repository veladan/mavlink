//
//  Database.swift
//  mavlink
//
//  Created by Daniel Vela on 18/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation

open class Database {
    fileprivate static var database = Database()
    
    open static func get() -> Database {
        return Database.database
    }
    
    var altitude: Int32?
    
    func setRawAltitude(_ altitude: Int32?) {
        self.altitude = altitude
    }
    
    func rawAltitude() -> Int32? {
        return self.altitude
    }
    
    var rssi: UInt8?
    
    func setRawRssi(_ rssi: UInt8?) {
        self.rssi = rssi
    }
    
    func rawRssi() -> UInt8? {
        return self.rssi
    }
    
    var battery: UInt16?
    
    func setRawBattery(_ battery: UInt16?) {
        self.battery = battery
    }
    
    func rawBattery() -> UInt16? {
        return self.battery
    }
    
    var speed: UInt16?
    
    func setRawSpeed(_ speed: UInt16?) {
        self.speed = speed
    }
    
    func rawSpeed() -> UInt16? {
        return self.speed
    }
    
    var latitude: Int32?
    
    func setRawLatitude(_ latitude: Int32?) {
        self.latitude = latitude
    }
    
    func rawLatitude() -> Int32? {
        return self.latitude
    }
    
    var longitude: Int32?
    
    func setRawLongitude(_ longitude: Int32?) {
        self.longitude = longitude
    }
    
    func rawLongitude() -> Int32? {
        return self.longitude
    }
    
    var seconds: UInt64?
    
    func setRawTime(_ seconds: UInt64?) {
        self.seconds = seconds
    }
    
    func rawTime() -> UInt64? {
        return self.seconds
    }
}
