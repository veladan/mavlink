//
//  MavlinkParser.swift
//  mavlink
//
//  Created by Daniel Vela on 17/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation

enum MavlinkSource {
    case channel_0
    case channel_1
    case file
}

class MavlinkParser {
    
    var msg = mavlink_message_t()
    var status = mavlink_status_t()
    
    func charReceived(_ char: UInt8, source: MavlinkSource) {
        if mavlink_parse_char(0, char, &msg, &status) != 0 {
            switch Int32(msg.msgid) {
            case MAVLINK_MSG_ID_GPS_RAW_INT:
                var gps_raw_int = mavlink_gps_raw_int_t()
                mavlink_msg_gps_raw_int_decode(&msg, &gps_raw_int)
                Database.get().setRawAltitude(gps_raw_int.alt)
                Database.get().setRawSpeed(gps_raw_int.vel)
                Database.get().setRawLatitude(gps_raw_int.lat)
                Database.get().setRawLongitude(gps_raw_int.lon)
                Database.get().setRawTime(gps_raw_int.time_usec)
                break
            case MAVLINK_MSG_ID_BATTERY2:
                var battery2 = mavlink_battery2_t()
                mavlink_msg_battery2_decode(&msg, &battery2)
                Database.get().setRawBattery(battery2.voltage)
            case MAVLINK_MSG_ID_RC_CHANNELS_RAW:
                var rc_channels_raw = mavlink_rc_channels_raw_t()
                mavlink_msg_rc_channels_raw_decode(&msg,&rc_channels_raw)
                Database.get().setRawRssi(rc_channels_raw.rssi)
            case MAVLINK_MSG_ID_SYS_STATUS:
                var mavlink_sys_status_raw = mavlink_sys_status_t()
                mavlink_msg_sys_status_decode(&msg, &mavlink_sys_status_raw)
                Database.get().setRawBattery(UInt16(mavlink_sys_status_raw.voltage_battery))
            case MAVLINK_MSG_ID_ALTITUDE:
                var mavlink_altitude_raw = mavlink_altitude_t()
                mavlink_msg_altitude_decode(&msg, &mavlink_altitude_raw)
                Database.get().setRawBattery(UInt16(mavlink_altitude_raw.altitude_relative))
                
            default:
                break
            }
        }
    }
    
    func didFinish() {
        Database.get().setRawAltitude(nil)
        Database.get().setRawSpeed(nil)
        Database.get().setRawSpeed(nil)
        Database.get().setRawLatitude(nil)
        Database.get().setRawLongitude(nil)
        Database.get().setRawBattery(nil)
        Database.get().setRawRssi(nil)
        Database.get().setRawTime(nil)
    }
}



//let fileArray = OSSystem.loadArrayFromResource("mavlink_sample_files")
//print(fileArray)
//
//do {
//    let path = NSBundle.mainBundle().pathForResource(fileArray[3] as? String, ofType: nil)
//    let file = try FileInputStream(fileName: path!)
//    var msg = mavlink_message_t()
//    var status = mavlink_status_t()
//    
//    while file.isDataAvailable() {
//        let (c, _) = file.readInt()
//        if mavlink_parse_char(0, c, &msg, &status) != 0 {
//            //                    print(status)
//            switch Int32(msg.msgid){
//            case MAVLINK_MSG_ID_HEARTBEAT:
//                var heartbeat = mavlink_heartbeat_t()
//                mavlink_msg_heartbeat_decode(&msg, &heartbeat)
//                //                        print(heartbeat)
//                break
//            case MAVLINK_MSG_ID_BATTERY2:
//                var battery2 = mavlink_battery2_t()
//                mavlink_msg_battery2_decode(&msg, &battery2)
//                //                        print(battery2)
//                break
//                //                    case MAVLINK_MSG_ID_BATTERY_STATUS:
//                //                        var battery_status = mavlink_battery_status_t()
//                //                        mavlink_msg_battery_status_decode(&msg, &battery_status)
//                ////                        print(battery_status)
//                //                    case MAVLINK_MSG_ID_ALTITUDE:
//                //                        var altitude = mavlink_altitude_t()
//                //                        mavlink_msg_altitude_decode(&msg, &altitude)
//                //                        print(altitude)
//            //                        break
//            case MAVLINK_MSG_ID_SYS_STATUS:
//                var sys_status = mavlink_sys_status_t()
//                mavlink_msg_sys_status_decode(&msg, &sys_status)
//                //                        print(sys_status)
//                break
//            case MAVLINK_MSG_ID_GPS_RAW_INT:
//                var gps_raw_int = mavlink_gps_raw_int_t()
//                mavlink_msg_gps_raw_int_decode(&msg, &gps_raw_int)
//                //                        print(gps_raw_int)
//                break
//                //                    case MAVLINK_MSG_ID_RADIO:
//                //                        var radio = mavlink_radio_t()
//                //                        mavlink_msg_radio_decode(&msg,&radio)
//                //                        print(radio)
//                //                    case MAVLINK_MSG_ID_RADIO_STATUS:
//                //                        var radio_status = mavlink_radio_status_t()
//                //                        mavlink_msg_radio_status_decode(&msg,&radio_status)
//                //                        print(radio_status)
//                //                    case MAVLINK_MSG_ID_RC_CHANNELS:
//                //                        var rc_channels = mavlink_rc_channels_t()
//                //                        mavlink_msg_rc_channels_decode(&msg,&rc_channels)
//            //                        print(rc_channels)
//            case MAVLINK_MSG_ID_RC_CHANNELS_RAW:
//                var rc_channels_raw = mavlink_rc_channels_raw_t()
//                mavlink_msg_rc_channels_raw_decode(&msg,&rc_channels_raw)
//                //                        print(rc_channels_raw)
//                break
//                //                    case MAVLINK_MSG_ID_RC_CHANNELS_SCALED:
//                //                        var rc_channels_scaled = mavlink_rc_channels_scaled_t()
//                //                        mavlink_msg_rc_channels_scaled_decode(&msg,&rc_channels_scaled)
//                //                        print(rc_channels_scaled)
//                //                        break
//                //                    case MAVLINK_MSG_ID_GPS2_RAW:
//                //                        var gps2_raw = mavlink_gps2_raw_t()
//                //                        mavlink_msg_gps2_raw_decode(&msg, &gps2_raw)
//                //                        print(gps2_raw)
//                //                    case MAVLINK_MSG_ID_GPS2_RTK:
//                //                        var gps2_rtk = mavlink_gps2_rtk_t()
//                //                        mavlink_msg_gps2_rtk_decode(&msg, &gps2_rtk)
//                //                        print(gps2_rtk)
//                //                        break
//                //                    case MAVLINK_MSG_ID_GPS_GLOBAL_ORIGIN:
//                //                        var gps_global_origin = mavlink_gps_global_origin_t()
//                //                        mavlink_msg_gps_global_origin_decode(&msg, &gps_global_origin)
//                //                        print(gps_global_origin)
//                
//            default:
//                break
//            }
//        }
//    }
//} catch FileInputStream.FileInputStreamError.FileCantBeOpened {
//    print("File can't be opened")
//} catch {
//    print("File fail to open")
//}
