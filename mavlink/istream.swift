//
//  istream.swift
//  mavlink
//
//  Created by Daniel Vela on 07/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation

protocol istream {
    func lengthAvailable() -> Int
    func isDataAvailable() -> Bool
    func readInt() -> (UInt8, Int) // char, currentStep
    func movePointerTo(_ position: Int)
}
