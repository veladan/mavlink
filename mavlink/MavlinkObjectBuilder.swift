//
//  MavlinkObjectBuilder.swift
//  mavlink
//
//  Created by Daniel Vela on 17/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation

class MavlinkObjectBuilder {
    
    func buildFileParser(_ fileInput: FileInputStream) -> FileChannel? {
        return FileChannel(input: fileInput, parser: MavlinkParser())
    }
}
