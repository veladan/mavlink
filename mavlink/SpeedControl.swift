//
//  SpeedControl.swift
//  mavlink
//
//  Created by Daniel Vela on 18/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit

class SpeedControl: MavlinkBarButtonItem {

    @objc override func timerTick() {
        
        guard let rawSpeed = Database.get().rawSpeed() else {
            setText("Spd: --")
            return
        }
        
        let speed = (Double(rawSpeed) * 3600.0) / (100.0 * 1000.0)
        let str = "Spd: ".appendingFormat("%.2f km/h", speed)
        setText(str)
    }
    
    func setText(_ str: String) {
        self.title = str
    }
}
