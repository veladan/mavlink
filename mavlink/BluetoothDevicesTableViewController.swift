//
//  BluetoothDevicesTableViewController.swift
//  mavlink
//
//  Created by Daniel Vela on 20/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit

class BluetoothDevicesTableViewController: UITableViewController, BluetoothManagerDelegate {
    internal func didConnectToPeripheral() { }

    var delegate: ViewController?
    var btManager: BluetoothManager?
    
    var devices: [String] = []
    
    override func viewDidLoad() {
         super.viewDidLoad()
        
        self.title = "Discovering..."
        
        btManager?.disconnectPeripheral()
        
        btManager?.startUp()
        btManager?.delegate = self
        
        addActivityViewToNavBar()
    }
    
    func addActivityViewToNavBar() {
        let uiBusy = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        uiBusy.hidesWhenStopped = true
        uiBusy.startAnimating()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: uiBusy)
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedDevice = devices[(indexPath as NSIndexPath).row]
        
        btManager?.connectPeripheralNamed(selectedDevice)
        tableView.deselectRow(at: indexPath, animated: true)
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BLDeviceCell", for: indexPath)
        
        cell.textLabel?.text = devices[(indexPath as NSIndexPath).row]
        
        return cell
    }

    // MARK
    func didDiscoverPeripheral(_ name: String) {
        let indexPath = IndexPath(row: devices.count, section: 0)
        tableView.beginUpdates()
        tableView.insertRows(at: [indexPath], with: UITableViewRowAnimation.bottom)
        devices.append(name)
        tableView.endUpdates()
    }
    
    func didUpdateState(_ string: String) {
        if string == "bluetooth on" {
            devices = []
        }
        // FIXME show the user any error or strange state
    }

}
