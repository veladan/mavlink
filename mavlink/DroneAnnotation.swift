//
//  DroneAnnotation.swift
//  mavlink
//
//  Created by Daniel Vela on 10/07/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import MapKit

class DroneAnnotation: NSObject, MKAnnotation {
    dynamic var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}