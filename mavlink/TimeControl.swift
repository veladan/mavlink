//
//  TimeControl.swift
//  mavlink
//
//  Created by Daniel Vela on 18/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit

extension TimeInterval {
    var minuteSecondMS: String {
        return String(format:"%d:%02d.%03d", minute, second, millisecond)
    }
    var minute: Int {
        return Int((self/60.0).truncatingRemainder(dividingBy: 60))
    }
    var second: Int {
        return Int(self.truncatingRemainder(dividingBy: 60))
    }
    var millisecond: Int {
        return Int((self*1000).truncatingRemainder(dividingBy: 1000 ))
    }
}

extension Int {
    var msToSeconds: Double {
        return Double(self) / 1000
    }
}


class TimeControl: UILabel {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startTimer()
    }
    
    func startTimer() {
        var _ = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(TimeControl.timerTick), userInfo: nil, repeats: true)
    }
    
    @objc func timerTick() {
        
        guard let rawSeconds = Database.get().rawTime() else {
            setLabelText("--:--.---")
            return
        }
    
        let d = Double(rawSeconds) / 1000000
        let microseconds = TimeInterval(d)
        let str = microseconds.minuteSecondMS
        setLabelText(str)
    }
    
    func setLabelText(_ text: String) {
        self.text = text
    }
}
