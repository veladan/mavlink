//
//  ostream.swift
//  mavlink
//
//  Created by Daniel Vela on 17/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation

protocol ostream {
    func writeInt(_ char: UInt8)
}
