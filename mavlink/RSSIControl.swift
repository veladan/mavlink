//
//  RSSIControl.swift
//  mavlink
//
//  Created by Daniel Vela on 18/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit

class RSSIControl: MavlinkBarButtonItem {
    
    @objc override func timerTick() {
        
        guard let rawRssi = Database.get().rawRssi() else {
            setText("rssi: --")
            return
        }
        
        let rssi = Double(rawRssi) / 2.55
        let rssiStr = String(format: "%d", UInt(rssi))
        let str = "rssi: \(rssiStr)%"
        setText(str)
    }
    
    func setText(_ str: String) {
        self.title = str
    }
}
