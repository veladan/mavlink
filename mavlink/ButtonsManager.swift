//
//  ButtonsManager.swift
//  mavlink
//
//  Created by Daniel Vela on 20/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit
import MapKit

class ButtonsManager: NSObject {

    func centerInUser(_ button: UIButton, mapView: MKMapView) {
        mapView.showsUserLocation = false
        mapView.showsUserLocation = true
    }
    
    func keepCentered(_ button: UIButton, mapView: MKMapView) {
        if button.isSelected == false {
            button.isSelected = true
            NotificationCenter.default.post(name: Notification.Name(rawValue: "KEEP_CENTERED_ON"), object: nil)
        } else {
            button.isSelected = false
            NotificationCenter.default.post(name: Notification.Name(rawValue: "KEEP_CENTERED_OFF"), object: nil)
        }
    }
    
    func centerInDrone(_ button: UIButton, mapView: MKMapView) {
        if let (latitude, longitude) = GPSControl.location() {
            let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let region = MKCoordinateRegion(center: location, span: ButtonsManager.userCoordinateSpan())
            mapView.setRegion(region, animated: true)
        }
    }
    
    static func userCoordinateSpan() -> MKCoordinateSpan {
        let latitude = UserDefaults.standard.double(forKey: ViewController.kUserDefinedSpanLatitude)
        let longitude = UserDefaults.standard.double(forKey: ViewController.kUserDefinedSpanLongitude)
        let span = MKCoordinateSpan(latitudeDelta: latitude, longitudeDelta: longitude)
        return span
    }
}
