//
//  BluetoothMavlinkDiscover.swift
//  mavlink
//
//  Created by Daniel Vela on 09/09/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import Foundation

/**
    This class manages an already connected peripheral to find all the
    characteristics.
    Also can register for notifications of the characteristics, read from them 
    and check if the readed data is mavlnk or not
*/
class BluetoothMavlinkDiscover {
    
}