//
//  GPSControl.swift
//  mavlink
//
//  Created by Daniel Vela on 18/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import MapKit

class GPSControl {

    var keepCentered = false
    
    var mapView: MKMapView
    
    init(map: MKMapView) {
        self.mapView = map
        startTimer()
        registerNotification()
    }
    
    func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(GPSControl.keepCenteredOn), name: NSNotification.Name(rawValue: "KEEP_CENTERED_ON"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GPSControl.keepCenteredOff), name: NSNotification.Name(rawValue: "KEEP_CENTERED_OFF"), object: nil)
    }
    
    @objc func keepCenteredOn() {
        keepCentered = true
    }
    
    @objc func keepCenteredOff() {
        keepCentered = false
    }

    func startTimer() {
        var _ = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(GPSControl.timerTick), userInfo: nil, repeats: true)
    }
    
    @objc func timerTick() {
        if let (latitude, longitude) = GPSControl.location() {
            setPosition(latitude, longitude)
            if keepCentered {
                setMapCentered(latitude,longitude)
            }
        } else {
            removeAnnotation()
        }
    }
    
    var annotation: DroneAnnotation?
    func setPosition(_ latitude: Double,_ longitude: Double) {
        let coordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        if annotation == nil {
            annotation = DroneAnnotation(coordinate: coordinate2D)
            mapView.addAnnotation(annotation!)
        } else {
            annotation?.coordinate = coordinate2D
        }
    }

    func removeAnnotation() {
        if let annotation = self.annotation {
            mapView.removeAnnotation(annotation)
            self.annotation = nil
        }
    }
    
    static func location() -> (Double, Double)? {
        guard let rawLatitude = Database.get().rawLatitude() else {
            return nil
        }
        guard let rawLongitude = Database.get().rawLongitude() else {
            return nil
        }
        let longitude = Double(rawLongitude) / 10000000
        let latitude = Double(rawLatitude) / 10000000
        return (latitude, longitude)
    }
    
    func setMapCentered(_ latitude: Double,_ longitude: Double) {
        let span = ButtonsManager.userCoordinateSpan()
        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
    }
}
