
//
//  ViewController.swift
//  mavlink
//
//  Created by Daniel Vela on 02/06/16.
//  Copyright © 2016 Daniel Vela. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, BluetoothManagerDelegate, CLLocationManagerDelegate, MKMapViewDelegate {

    lazy var buttonsManager = ButtonsManager()
    lazy var bluetoothManager = BluetoothManager()
    
    @IBOutlet weak var rssiControl: RSSIControl!
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var progressControl: ProgressControl!

    @IBOutlet weak var stopButton: UIBarButtonItem!
    
    @IBOutlet weak var connectButton: UIBarButtonItem!
    
    var gpsControl: GPSControl?
    
    let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        locationManager.requestWhenInUseAuthorization()
        
        stopButton.isEnabled = false
        
        gpsControl = GPSControl(map: self.mapView)
    }

    @IBAction func rssiPressed(_ sender: AnyObject) {
        
    }

    @IBAction func connectPressed(_ sender: AnyObject) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        bluetoothManager.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override open func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BlutoothStartDiscover" {
            let controller = segue.destination as? BluetoothDevicesTableViewController
            controller?.delegate = self
            controller?.btManager = self.bluetoothManager
            
            if connectButton.title == "Disconnect" {
               connectButton.title = "Connect"
            }
        }
        if segue.identifier == "FilesTableSegue" {
            stopReplay()
            let controller = segue.destination as? FileTableViewController
            controller?.delegate = self
        }
    }
    
    // MARK BluetoothManager Delegate
    
    func didConnectToPeripheral() {
        print("connected")
        connectButton.title = "Disconnect"
        
        bluetoothManager.output = MavlinkChannel(parser: MavlinkParser(), source: MavlinkSource.channel_0)
        
        centerInDroneWithDelay()
    }
    
    func didDiscoverPeripheral(_ name: String) {
        
    }
    func didUpdateState(_ string: String) {
        
    }

    // MARK Buttons pressed
    
    @IBAction func centerInUser(_ sender: AnyObject) {
        buttonsManager.centerInUser(sender as! UIButton, mapView: self.mapView)
    }
    
    @IBAction func keepCentered(_ sender: AnyObject) {
        buttonsManager.keepCentered(sender as! UIButton, mapView: self.mapView)
    }
    
    
    @IBAction func centerInDrone(_ sender: AnyObject) {
        buttonsManager.centerInDrone(sender as! UIButton, mapView: self.mapView)
    }
    
    func centerInDroneWithDelay() {
        self.perform(#selector(ViewController.centerInDrone(_:)), with: nil, afterDelay: 1.0)
    }

    // MARK File replay 
    
    var fileChannel: FileChannel?
    func fileSelected(_ fileInput: FileInputStream) {
        stopButton.isEnabled = true
        fileChannel = MavlinkObjectBuilder().buildFileParser(fileInput)
        guard let fileChannel = fileChannel else { return }
        fileChannel.delegate = self.progressControl
        DispatchQueue.global(qos: .background).async {
            fileChannel.run()
        }
        centerInDroneWithDelay()
    }
    
    @IBAction func userChangedProgress(_ sender: AnyObject) {
        let position = self.progressControl.value
        fileChannel?.movePointerTo(position)
    }
    
    
    @IBAction func stopButtonPressed(_ sender: AnyObject) {
        stopReplay()
    }
    
    func stopReplay() {
        stopButton.isEnabled = false
        fileChannel?.stop()
        progressControl.stop()
    }
    
    // MARK Map View Delegate
    
    var locatingUser = true
    
    func mapViewWillStartLocatingUser(_ mapView: MKMapView) {
        locatingUser = true
    }
    func mapViewDidStopLocatingUser(_ mapView: MKMapView) {
        locatingUser = false
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        let span = ButtonsManager.userCoordinateSpan()
        let location = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        locatingUser = false
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is DroneAnnotation {
            let annotationView = DroneAnnotationView(annotation: annotation, reuseIdentifier: "DroneAnnotationView")
            annotationView.image = UIImage(named: "drone_ico")
            return annotationView
        }
        return nil
    }
    
    static let kUserDefinedSpanLatitude = "kUserDefinedSpanLatitude"
    static let kUserDefinedSpanLongitude = "kUserDefinedSpanLongitude"
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if !locatingUser {
            let span: MKCoordinateSpan = mapView.region.span
            UserDefaults.standard.set(span.latitudeDelta, forKey: ViewController.kUserDefinedSpanLatitude)
            UserDefaults.standard.set(span.longitudeDelta, forKey: ViewController.kUserDefinedSpanLongitude)
            UserDefaults.standard.synchronize()
        }
    }
}

